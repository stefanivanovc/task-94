import EventEmitter from "eventemitter3";

export default class Application extends EventEmitter {
  static get events() {
    return {
      READY: "ready",
    };
  }

  constructor() {
    super();
    this.emojis = [];
    this.banana = "🍌";
    this.emit(Application.events.READY);
  }

  setEmojis(emojis) {
    this.emojis = emojis;
    console.log(emojis);
  }

  addBananas(emojis) {
     this.emojis.map (m => m + this.banana);
     const newAr = document.createElement(emojis);
  }
}
